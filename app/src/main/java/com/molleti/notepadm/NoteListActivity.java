package com.molleti.notepadm;

import android.app.ActivityOptions;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBar;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.molleti.notepadm.data.NotepadContract;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;

public class NoteListActivity extends BaseActivity implements LoaderManager.LoaderCallbacks<Cursor>, AbsListView.MultiChoiceModeListener {
    private static final int NOTES_LOADER = 1;
    private static final String[] PROJECTION =  {
            NotepadContract.NoteEntry._ID,
            NotepadContract.NoteEntry.COLUMN_TITLE
    };
    @Bind(R.id.listView) ListView listView;
    @Bind(R.id.fab) FloatingActionButton fab;

    private SimpleCursorAdapter adapter;

    ArrayList<Long> selectedIds = new ArrayList<Long>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_list);
        ButterKnife.bind(this);

        String[] from = new String[] { NotepadContract.NoteEntry.COLUMN_TITLE };
        int[] to = new int[] { android.R.id.text1 };
        adapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_activated_1, null, from, to, 0);
        listView.setAdapter(adapter);
        getSupportLoaderManager().initLoader(NOTES_LOADER, null, this);

        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case NOTES_LOADER:
                return new CursorLoader(this, NotepadContract.NoteEntry.CONTENT_URI, PROJECTION, null, null, null);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.changeCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.changeCursor(null);
    }

    @Override
    public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
        if (checked) selectedIds.add(id);
        else selectedIds.remove(id);
        mode.setTitle(String.valueOf(selectedIds.size()));
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cab, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                for (long id : selectedIds) {
                    getContentResolver().delete(
                            ContentUris.withAppendedId(NotepadContract.NoteEntry.CONTENT_URI, id),
                            null,
                            null
                    );
                }
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        selectedIds.clear();
    }

    @OnClick(R.id.fab)
    public void onFabClick(View view) {
        Intent intent = new Intent(this, PopupActivity.class);
        intent.setAction(PopupActivity.ACTION_NEW);
        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation
                (NoteListActivity.this, view, getString(R.string.transition_morph_view));
        startActivity(intent, options.toBundle());
    }

    @OnItemClick(R.id.listView)
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(this, NoteViewActivity.class);
        intent.setData(ContentUris.withAppendedId(NotepadContract.NoteEntry.CONTENT_URI, id));
        intent.setAction(NoteViewActivity.ACTION_VIEW);
        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation
                (NoteListActivity.this, view, getString(R.string.transition_morph_view));
        startActivity(intent, options.toBundle());
    }

    private void setupToolbar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);
    }
}
