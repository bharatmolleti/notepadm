package com.molleti.notepadm;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.transition.ArcMotion;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;

import com.molleti.notepadm.data.NotepadContract;
import com.molleti.notepadm.util.MorphDialogToFab;
import com.molleti.notepadm.util.MorphDialogToListItem;
import com.molleti.notepadm.util.MorphFabToDialog;
import com.molleti.notepadm.util.MorphListItemToDialog;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NoteViewActivity extends Activity {
    @Bind(R.id.editText_title)
    AppCompatEditText titleEditText;
    @Bind(R.id.editText_description)
    AppCompatEditText descriptionEditText;
    @Bind(R.id.fab) FloatingActionButton fab;

    private Uri note;

    boolean isDismissing = false;
    private ViewGroup container;

    public static final String ACTION_VIEW = "action_view";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_view);
        ButterKnife.bind(this);


        final Intent intent = getIntent();
        if (intent.getAction().equals(ACTION_VIEW)) {
            setupSharedElementTransitionsListItem(this, container);
            note = intent.getData();
            Cursor cursor = getContentResolver().query(note, null, null, null, null);
            cursor.moveToFirst();
            String title = cursor.getString(cursor.getColumnIndexOrThrow(NotepadContract.NoteEntry.COLUMN_TITLE));
            String description = cursor.getString(cursor.getColumnIndexOrThrow(NotepadContract.NoteEntry.COLUMN_DESCRIPTION));
            cursor.close();

            titleEditText.setText(title);
            descriptionEditText.setText(description);

            fab.setImageResource(R.drawable.ic_action_edit);
        }
        container = (ViewGroup) findViewById(R.id.container);
    }

    public void setupSharedElementTransitionsFab(@NonNull Activity activity,
                                                 @Nullable View target,
                                                 int dialogCornerRadius) {
        ArcMotion arcMotion = new ArcMotion();
        arcMotion.setMinimumHorizontalAngle(50f);
        arcMotion.setMinimumVerticalAngle(50f);
        int color = ContextCompat.getColor(activity, R.color.colorAccent);
        Interpolator easeInOut =
                AnimationUtils.loadInterpolator(activity, android.R.interpolator.fast_out_slow_in);
        MorphFabToDialog sharedEnter = new MorphFabToDialog(color, dialogCornerRadius);
        sharedEnter.setPathMotion(arcMotion);
        sharedEnter.setInterpolator(easeInOut);
        MorphDialogToFab sharedReturn = new MorphDialogToFab(color);
        sharedReturn.setPathMotion(arcMotion);
        sharedReturn.setInterpolator(easeInOut);
        if (target != null) {
            sharedEnter.addTarget(target);
            sharedReturn.addTarget(target);
        }
        activity.getWindow().setSharedElementEnterTransition(sharedEnter);
        activity.getWindow().setSharedElementReturnTransition(sharedReturn);
    }

    public void setupSharedElementTransitionsListItem(@NonNull Activity activity,
                                                    @Nullable View target) {
        ArcMotion arcMotion = new ArcMotion();
        arcMotion.setMinimumHorizontalAngle(50f);
        arcMotion.setMinimumVerticalAngle(50f);
        int color = ContextCompat.getColor(activity, R.color.colorAccent);
        Interpolator easeInOut =
                AnimationUtils.loadInterpolator(activity, android.R.interpolator.fast_out_slow_in);
        MorphListItemToDialog sharedEnter = new MorphListItemToDialog(color);
        sharedEnter.setPathMotion(arcMotion);
        sharedEnter.setInterpolator(easeInOut);
        MorphDialogToListItem sharedReturn = new MorphDialogToListItem(color);
        sharedReturn.setPathMotion(arcMotion);
        sharedReturn.setInterpolator(easeInOut);
        if (target != null) {
            sharedEnter.addTarget(target);
            sharedReturn.addTarget(target);
        }
        activity.getWindow().setSharedElementEnterTransition(sharedEnter);
        activity.getWindow().setSharedElementReturnTransition(sharedReturn);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    public void dismiss(View view) {
        isDismissing = true;
        setResult(Activity.RESULT_CANCELED);
        finishAfterTransition();
    }

    @Override
    public void onBackPressed() {
        dismiss(null);
    }

   @OnClick(R.id.fab)
   public void onFabClick(View view) {
       Intent intent = new Intent(this, PopupActivity.class);
       intent.setData(note);
       intent.setAction(PopupActivity.ACTION_EDIT);
       ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation
               (NoteViewActivity.this, view, getString(R.string.transition_morph_view));
       startActivity(intent, options.toBundle());
       finish();
    }

}